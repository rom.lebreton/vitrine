const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const fs = require('fs');
const path = require('path');
const pg = require('pg'); 

const config = { // Configuration pour l'accès BDD Postgres
    user: 'postgres',
    password: 'romain',
    database: 'vitrine',
    port: 5432
};

const client = new pg.Client(config);
client.connect(); // Connexion à la BDD

app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    res.setHeader('Access-Control-Allow-Headers', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,PATCH,POST,DELETE');
    next();
});

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

//--------------------------Objet
app.get('/objet', function(req, res){
    client.query('SELECT * FROM objet o, langue_objet lo, langue l, categorie c, categorie_langue cl ' +
    'WHERE o.uuidobjet = lo.uuidobjet '+
    'AND o.uuidcategorie = c.uuidcategorie '+
    'AND c.uuidcategorie = cl.uuidcategorie ' +
    'AND l.isolangue = lo.isolangue '+
    'ORDER BY o.uuidobjet ASC', function(err, result){
        res.send(result.rows);
    })  
})

.get('/objet/:id', function(req, res){
    client.query('SELECT * FROM objet o, langue_objet lo, langue l, categorie c, categorie_langue cl ' +
    'WHERE o.uuidobjet = lo.uuidobjet '+
    'AND o.uuidcategorie = c.uuidcategorie '+
    'AND c.uuidcategorie = cl.uuidcategorie ' +
    'AND l.isolangue = lo.isolangue '+
    'AND o.uuidobjet = $1', [req.params.id], function(err, result){
        res.send(result.rows);
    })  
})

.post('/objet', function(req, res){
    console.log('POST OBJET : ', req.body.nom, req.body.description, req.body.emplacement, req.body.categorie, req.body.langue, req.body.fabrication)
    client.query('INSERT INTO objet VALUES (nextval(\'auto_increment_objet\'), current_date, null, true, $1, $2, $3)', [req.body.fabrication, req.body.emplacement, req.body.categorie])
    client.query('INSERT INTO langue_objet VALUES ($1, $2, $3, (SELECT MAX(uuidobjet) FROM objet))', [req.body.nom, req.body.description, req.body.langue])
})

.put('/objet/:id', function(req, res){
    client.query('UPDATE langue_objet SET nomobjet = $1, descriptionobjet = $2 WHERE uuidobjet = $3', [req.body.libelle, req.body.description, req.params.id])
    .then(result => res.send(result.rows))
})

.delete('/objet/:id', function(req, res){
    client.query('DELETE FROM objet WHERE uuidobjet = $1', [req.params.id])
    .then(result => res.send(result.rows))
})

//-------------------------Langue
.get('/langue', function(req, res){
    client.query('SELECT * FROM langue', function(err, result){
        res.send(result.rows);
    })
})

.get('/langue/:id', function(req, res){
    client.query('SELECT * FROM langue WHERE isolangue = $1', [req.params.id], function(err, result){
        res.send(result.rows);
    })
})

.post('/langue', function(req, res){
    client.query('INSERT INTO langue (isolangue, libellelangue) VALUES ($1, $2) RETURNING *', [req.body.iso, req.body.libelle])
    .then(result => res.send(result.rows))
})

.put('/langue/:id', function(req, res){
    client.query('UPDATE langue SET isolangue = $1 ,libellelangue = $2 WHERE isolangue = $3 RETURNING *', [req.body.iso, req.body.libelle, req.params.id])
    .then(result => res.send(result.rows))
})

.delete('/langue/:id', function(req, res){
    client.query('DELETE FROM langue WHERE isolangue = $1 RETURNING *', [req.params.id])
    .then(result => res.send(result.rows))
})

//---------------Catégorie
.get('/categorie', function(req, res){
    client.query('SELECT * FROM categorie c, categorie_langue cl, langue l '+
    'WHERE cl.isolangue = l.isolangue AND cl.uuidcategorie = c.uuidcategorie '+
    'ORDER BY c.uuidcategorie ASC', function(err, result){
        res.send(result.rows);
    })
})

.get('/categorie/:id', function(req, res){
    client.query('SELECT * FROM categorie c, categorie_langue cl, langue l WHERE cl.isolangue = l.isolangue AND cl.uuidcategorie = c.uuidcategorie AND c.uuidcategorie = $1 AND cl.isolangue = $2', [req.params.id, req.query.langue], function(err, result){
        res.send(result.rows);
        console.log(result)
    }) 
})

.post('/categorie', function(req, res){
    console.log('POST :', req.body.nom, req.body.langue)
    client.query('INSERT INTO categorie VALUES (nextval(\'auto_increment_categorie\'), $1)', [req.body.description])
    client.query('INSERT INTO categorie_langue VALUES ($1, $2, (SELECT MAX(uuidcategorie) from categorie))', [req.body.nom, req.body.langue])
})

.put('/categorie/:id', function(req, res){
    client.query('UPDATE categorie SET descriptioncategorie = $1 WHERE uuidcategorie = $2 RETURNING *', [req.body.description, req.params.id])
    .then(result => res.send(result.rows))
    client.query('UPDATE categorie_langue ' +
    'SET libellecategorie = $1, isolangue = $2 ' +
    'WHERE uuidcategorie = $3 RETURNING *', [req.body.nom, req.body.langue, req.params.id])
    .then(result => res.send(result.rows))
})

.delete('/categorie/:id', function(req, res){
    client.query('DELETE FROM categorie_langue WHERE uuidcategorie = $1', [req.params.id])
    client.query('DELETE FROM categorie WHERE uuidcategorie = $1', [req.params.id])
})

//---------------Vitrine (Configuration)
.post('/vitrine', function(req, res){
    console.log('POST Vitrine :', req.body.nom, req.body.objet)
    client.query('INSERT INTO vitrine VALUES (nextval(\'auto_increment_vitrine\'), $1, 1)', [req.body.nom])
    for(var i = 0; i< req.body.objet.length; i++) {
        console.log('POST : ', req.body.objet[i], i)
        client.query('INSERT INTO objet_vitrine VALUES ($1, $2, (SELECT MAX(uuidvitrine) from vitrine))', [i, req.body.objet[i]])
    }
})

.delete('/vitrine/:id', function(req, res){
    console.log('DELETE :', req.params.id)
    client.query('DELETE FROM objet_vitrine WHERE uuidvitrine = $1', [req.params.id])
    client.query('DELETE FROM vitrine WHERE uuidvitrine = $1', [req.params.id])
})

//----------------Vitrine (Visiteur)
.get('/vitrine', function(req, res){
    console.log('GET ', req.params.id)
    client.query('SELECT * FROM vitrine v, objet_vitrine ov, objet o '+
    'WHERE o.uuidobjet = ov.uuidobjet '+
    'AND ov.uuidvitrine = v.uuidvitrine ')
    .then(result => res.send(result.rows))
})

.get('/vitrine/:id', function(req, res){
    console.log('GET ', req.params.id)
    client.query('SELECT * FROM objet o, objet_vitrine ov, vitrine v, langue_objet lo, langue l '+
    'WHERE o.uuidobjet = ov.uuidobjet '+
    'AND ov.uuidvitrine = v.uuidvitrine '+
    'AND o.uuidobjet = lo.uuidobjet ' +
    'AND lo.isolangue = l.isolangue '+
    'AND v.uuidvitrine = $1 '+
    'ORDER BY ov.positionobjet DESC',[req.params.id])
    .then(result => res.send(result.rows))
})

//---------------Metier
.get('/metier', function(req, res){
    client.query('SELECT * FROM metier')
    .then(result => res.send(result.rows))
})

.post('/metier', function(req, res){
    console.log('POST : ', req.body.libelle)
    client.query('INSERT INTO metier VALUES (nextval(\'auto_increment_metier\'), $1)', [req.body.libelle])
    .then(result => res.send(result.rows))
})

//---------------Personne
.get('/personne', function(req, res){
    client.query('SELECT * FROM personne p, metier_personne mp, metier m '+
    'WHERE p.uuidpersonne = mp.uuidpersonne '+
    'AND m.uuidmetier = mp.uuidmetier')
    .then(result => res.send(result.rows))
})

.post('/personne', function(req, res){
    client.query('INSERT INTO personne VALUES (nextval(\'auto_increment_personne\'), $1, $2, $3, $4)', [req.body.nom, req.body.prenom, req.body.date, req.body.lieunaissance])
    .then(result => res.send(result.rows))
    client.query('INSERT INTO metier_personne VALUES ($1, (SELECT MAX(uuidpersonne) from personne))', [req.body.metier])
})

//---------------Image
.get('/image', function(req, res){
    client.query('SELECT * FROM objet o, image_objet io, image i, vitrine v, objet_vitrine ov '+
    'WHERE o.uuidobjet = io.uuidobjet '+
    'AND io.uuidimage = i.uuidimage '+
    'AND o.uuidobjet = ov.uuidobjet '+
    'AND ov.uuidvitrine = v.uuidvitrine')
    .then(result => res.send(result.rows))
})

//---------------Upload
.post('/upload', function (req, res) {
    console.log('UPLOAD : ', req.files)
    console.log('UPLOAD : ', req.body)
    fs.readFile(req.files.file.path, function (err, data) {
    const imageName = req.files.file.name
    if(!imageName){
        console.log("There was an error")
    } else {
        const newPath = __dirname + "/src/assets/" + imageName;
        fs.writeFile(newPath, data, function (err) {
        res.redirect("/src/assets/" + imageName);
        })
    }
    })
})

//---------------Configuration Port
.listen(3000, function(){ // Ecoute sur le port 3000
    console.log('Serveur en marche sur le port 3000');
});
