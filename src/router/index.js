import Vue from 'vue'
import Router from 'vue-router'
import Accueil from '@/components/Accueil'
import Personne from '@/components/configuration/Personne'
import Objets from '@/components/configuration/Objets'
import Categories from '@/components/configuration/Categories'
import VitrineConfig from '@/components/configuration/Vitrine'
import Langues from '@/components/configuration/Langues'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'accueil',
      component: Accueil
    },
    {
      path: '/configuration/personne',
      name: 'personne',
      component: Personne
    },
    {
      path: '/configuration/objets',
      name: 'objets',
      component: Objets
    },
    {
      path: '/configuration/categories',
      name: 'categories',
      component: Categories
    },
    {
      path: '/configuration/vitrine',
      name: 'vitrineconfig',
      component: VitrineConfig
    },
    {
      path: '/configuration/langues',
      name: 'langues',
      component: Langues
    }
  ],
  mode: 'history'
})
